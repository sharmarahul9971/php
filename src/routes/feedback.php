<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \Tuupola\Middleware\CorsMiddleware;

// $app = new \Slim\App;

$app = new Slim\App;

// $app->options('/{routes:.+}', function ($request, $response, $args) {
//     return $response;
// });

// $app->add(function ($req, $res, $next) {
//     $response = $next($req, $res);
//     return $response
//             ->withHeader('Access-Control-Allow-Origin', '*')
//             ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
//             ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
// });

$app->add(new Tuupola\Middleware\CorsMiddleware([
    "origin" => ["http://localhost:3000"],
    "methods" => ["GET", "POST", "PUT", "PATCH", "DELETE"],
    "headers.allow" => ["Accept", "Content-Type"],
    "headers.expose" => [],
    "credentials" => false,
    "cache" => 0
]));

// Get All Feedbacks
$app->get('/api/feedbacks', function(Request $request, Response $response){
    $sql = "SELECT * FROM feedbacks";
    try{
        // Get DB Object
        $db = new db();
        // Connect
        $db = $db->connect();
        $stmt = $db->query($sql);
        $feedbacks = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        echo json_encode($feedbacks);
    } catch(PDOException $e){
        echo '{"error": {"text": '.$e->getMessage().'}';
    }
});
// Get Single Feedback
// $app->get('/api/feedback/{id}', function(Request $request, Response $response){
//     $id = $request->getAttribute('id');
//     $sql = "SELECT * FROM feedbacks WHERE id = $id";
//     try{
//         // Get DB Object
//         $db = new db();
//         // Connect
//         $db = $db->connect();
//         $stmt = $db->query($sql);
//         $feedback = $stmt->fetch(PDO::FETCH_OBJ);
//         $db = null;
//         echo json_encode($feedback);
//     } catch(PDOException $e){
//         echo '{"error": {"text": '.$e->getMessage().'}';
//     }
// });
// Add Feedback
$app->post('/api/feedback/add', function(Request $request, Response $response){
    $feedback = $request->getParam('feedback');
    // $fingerprint = $request->getParam('fingerprint');

    $sql = "INSERT INTO feedbacks (feedback) VALUES (:feedback)";
    try{
        // Get DB Object
        $db = new db();
        // Connect
        $db = $db->connect();
        $stmt = $db->prepare($sql);
        $stmt->bindParam(':feedback', $feedback);
        // $stmt->bindParam(':fingerprint', $fingerprint);
        $stmt->execute();
        echo '{"notice": {"text": "Feedback Added"}';
    } catch(PDOException $e){
        echo '{"error": {"text": '.$e->getMessage().'}';
    }
});
// Update Customer
// $app->put('/api/customer/update/{id}', function(Request $request, Response $response){
//     $id = $request->getAttribute('id');
//     $feedback = $request->getParam('feedback');
    
//     $sql = "UPDATE feedbacks SET
// 				feedback 	= :feedback
// 			WHERE id = $id";
//     try{
//         // Get DB Object
//         $db = new db();
//         // Connect
//         $db = $db->connect();
//         $stmt = $db->prepare($sql);
//         $stmt->bindParam(':feedback', $feedback);
        
//         $stmt->execute();
//         echo '{"notice": {"text": "Customer Updated"}';
//     } catch(PDOException $e){
//         echo '{"error": {"text": '.$e->getMessage().'}';
//     }
// });
// // Delete Customer
// $app->delete('/api/customer/delete/{id}', function(Request $request, Response $response){
//     $id = $request->getAttribute('id');
//     $sql = "DELETE FROM feedbacks WHERE id = $id";
//     try{
//         // Get DB Object
//         $db = new db();
//         // Connect
//         $db = $db->connect();
//         $stmt = $db->prepare($sql);
//         $stmt->execute();
//         $db = null;
//         echo '{"notice": {"text": "Customer Deleted"}';
//     } catch(PDOException $e){
//         echo '{"error": {"text": '.$e->getMessage().'}';
//     }
// });